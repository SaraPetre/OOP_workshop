
from cmath import log
from functools import wraps
import sys

# def add_logging(fn):
#     @wraps (fn)
#     def logged_fn():
#         print("entering logging closure")
#         print("logging")
#         print("exiting logging closure")
#         return fn()
#     return logged_fn

def log_to_file(log_file):
    def add_cleanup(fn):
        @wraps (fn)
        def logging():
            with open("log.txt", 'a') as log:
                log.write("log from funktion1\n")
                #print("entering cleanup closure")
                return_value = fn()
                log.write("log from funktion2")
                #print("cleanup")
                #print("exiting cleanup closure")
                print("Open log.txt-file to find output")
                return return_value
        return logging
    return add_cleanup

# Logs calls to my_fn to log1.log 
# @my_decorator("log1.log")
#     def my_fn():
#         print("a")

#@add_cleanup
#@add_logging
@log_to_file("log.txt")
#@my_decorator("log1.log")
def a():
    '''
    This is function a's docstring
    '''
    # print("a")
    return "Hann inte mera!"



# a= add_logging(a)
# a= add_cleanup(a)



